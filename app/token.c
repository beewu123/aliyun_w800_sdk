/*****************************************************************************
*
* File Name : wm_mqtt_cloud.c
*
* Description: mqtt cloud function
*
* Copyright (c) 2015 Winner Micro Electronic Design Co., Ltd.
* All rights reserved.
*
* Author : LiLimin
*
* Date : 2019-3-24
*****************************************************************************/
#include <string.h>
#include "wm_include.h"
#include "wm_crypto_hard.h"
#include "sha1.h"
#include "component/kv/include/aos/kv.h"

char g_ClientId[128] =   {'\0'};
char g_UserName[128] =   {'\0'};
char g_Password[128] =   {'\0'};
char g_AliyunUrl[128] =  {'\0'};
char g_Get_Topic[128]  =  {'\0'};
// char g_Post_Topic[128]  =  {'\0'};
char g_Event_Topic[128]  =  {'\0'};

void build_mqtt_auth(u32 timestamp)
{
    char pk[32 + 1];
    char dn[20 + 1];
    char ds[64 + 1];
    // char ps[64 + 1];
    // char pid[16 + 1];
    int size = 0;
    int ret = 0;

    size = sizeof(pk);
    ret = aos_kv_get("hal_devinfo_pk", pk, &size);
    if(ret != 0)
        pk[0] = '\0';

    size = sizeof(dn);
    ret = aos_kv_get("hal_devinfo_dn", dn, &size);
    if(ret != 0)
        dn[0] = '\0';

    size = sizeof(ds);
    ret = aos_kv_get("hal_devinfo_ds", ds, &size);
    if(ret != 0)
        ds[0] = '\0';

    // size = sizeof(ps);
    // ret = aos_kv_get("hal_devinfo_ps", ps, &size);
    // if(ret != 0)
    //     ps[0] = '\0';
    
    // size = sizeof(pid);
    // ret = aos_kv_get("hal_devinfo_pid", pid, &size);
    // if(ret != 0)
    //     pid[0] = '\0';

    memset(g_ClientId,'\0',128);
    memset(g_UserName,'\0',128);
    memset(g_Password,'\0',128);
    memset(g_AliyunUrl,'\0',128);

    // 需要毫秒级时间戳
    char timestamp_str[32] = {'\0'};
    sprintf(timestamp_str,"%d232", timestamp);
    // printf("timestamp_str:%s\n",timestamp_str);
    
    // Mqtt ClientId
    sprintf(g_ClientId,"%s.%s|securemode=2,signmethod=hmacsha1,timestamp=%s|", pk, dn, timestamp_str);
    // printf("--->g_ClientId:%s\n",g_ClientId);
    
    // Mqtt UserName
    sprintf(g_UserName,"%s&%s", dn, pk);
    // printf("--->g_UserName:%s\n",g_UserName);

    // Mqtt Password
    char contentStr[128] = {'\0'};
    size = 0;
    size += sprintf(contentStr + size, "clientId%s.%s", pk, dn);
    size += sprintf(contentStr + size, "deviceName%s", dn);
    size += sprintf(contentStr + size, "productKey%s", pk);
    size += sprintf(contentStr + size, "timestamp%s", timestamp_str);
    // printf("--->contentStr:%s\n",contentStr);
    // printf("--->ds:%s",ds);
    u8 result[20] = {0};
    hmac_sha1((u8 *)ds, strlen(ds), (u8 *)contentStr, strlen(contentStr), (u8 *)result);
    for(u8 i=0;i<20;i++){
        sprintf(g_Password+2*i, "%02x",result[i]);
    }
    // printf("--->g_Password:%s\n",g_Password);


    // Mqtt AliyunUrl
    sprintf(g_AliyunUrl, "%s.iot-as-mqtt.cn-shanghai.aliyuncs.com", pk);
    // printf("--->g_AliyunUrl: %s\n",g_AliyunUrl);

    // Mqtt Get Topic
    sprintf(g_Get_Topic, "/sys/%s/%s/user/get", pk, dn);
    // printf("--->g_Get_Topic: %s\n",g_Get_Topic);

    // Mqtt Post Topic
    // sprintf(g_Post_Topic, "/sys/%s/%s/user/update", pk, dn);
    // printf("--->g_Post_Topic:%s\n",g_Post_Topic);

    // Mqtt Event Topic
    sprintf(g_Event_Topic, "/sys/%s/%s/thing/event/property/post", pk, dn);
    // printf("--->g_Event_Topic:%s\n",g_Event_Topic);


    // sprintf(g_ClientId,"a18kg0GQiT9.w800|securemode=2,signmethod=hmacsha256,timestamp=1652875361767|");
    // sprintf(g_UserName,"w800&a18kg0GQiT9");
    // sprintf(g_Password,"7720e0513cf9012b329f0965faf1c07f15c37f293ded765cf47d655a51d36d9a");
    // sprintf(g_ClientId,"a18kg0GQiT9.w800|securemode=2,signmethod=hmacsha1,timestamp=1652882735434|");
    // sprintf(g_UserName,"w800&a18kg0GQiT9");
    // sprintf(g_Password,"442E7FB1080EB3B6A173ED00D14AEBDF1722E0EA");
    // printf("--->g_ClientId:%s\n",g_ClientId);
    // printf("--->g_UserName:%s\n",g_UserName);
    // printf("--->g_Password:%s\n",g_Password);
    // sprintf(g_AliyunUrl, "a18kg0GQiT9.iot-as-mqtt.cn-shanghai.aliyuncs.com");
}

