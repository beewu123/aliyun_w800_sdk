#ifndef _MULTI_LIGHT_H_
#define _MULTI_LIGHT_H_
#include "wm_include.h"
#include "stdint.h"
#include "string.h"

typedef struct Light {
	uint32_t ticks;
	uint32_t keep_ticks;
	uint32_t last_ticks;
	uint16_t high_level_ticks;
	uint16_t low_level_ticks;
	uint8_t  state : 6;
	uint8_t  active_level : 1;
	uint8_t  last_level  : 1;
	enum tls_io_name  light_gpio;
	struct Light* next;
}Light;


#ifdef __cplusplus
extern "C" {
#endif


void light_init(struct Light* handle, enum tls_io_name gpio_pin, uint8_t active_level);

int light_start(struct Light* handle);

void light_stop(struct Light* handle);

void light_blink(struct Light* handle,u16 high,u16 low);

void light_blink_timeout(struct Light* handle,u16 high,u16 low,u32 timeout);

void light_on(struct Light* handle);

void light_on_timeout(struct Light* handle,u32 timeout);

void light_off(struct Light* handle);


#ifdef __cplusplus
}
#endif

#endif
