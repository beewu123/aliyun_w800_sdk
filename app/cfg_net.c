/***************************************************************************** 
* 
* File Name : cfg_net.c
* 
* Description: 注册配置网络模块 
* 
* Copyright (c) 2014 Winner Micro Electronic Design Co., Ltd. 
* All rights reserved. 
* 
* Author : isme
* 
* Date : 2022-5-17
*****************************************************************************/ 
#include "wm_include.h"
#include "component/led/multi_light.h"
#include "component/button/multi_button.h"
static struct Light led_net_status;
static struct Button btn_net_cfg;

static void BTN0_SINGLE_Click_Handler(void* btn)
{
	//do something...
	printf("---> %s\r\n",__func__);
}


static void BTN0_LONG_PRESS_START_Handler(void* btn)
{
	//do something...
	printf("---> %s\r\n",__func__);
	tls_wifi_disconnect();
    extern int demo_bt_enable();
	demo_bt_enable();
	tls_os_time_delay(HZ/10);
	tls_wifi_set_oneshot_flag(4);
	light_blink(&led_net_status, 100, 100);
}

static void con_net_status_changed_event(u8 status )
{
    switch(status)
    {
    case NETIF_WIFI_JOIN_SUCCESS:
        printf("---> NETIF_WIFI_JOIN_SUCCESS\n");
        break;
    case NETIF_WIFI_JOIN_FAILED:
        printf("---> NETIF_WIFI_JOIN_FAILED\n");
        light_off(&led_net_status);
        break;
    case NETIF_WIFI_DISCONNECTED:
        printf("---> NETIF_WIFI_DISCONNECTED\n");
		light_off(&led_net_status);
        break;
    case NETIF_IP_NET_UP:
    {
		light_on(&led_net_status);
        // struct tls_ethif *tmpethif = tls_netif_get_ethif();
        // print_ipaddr(&tmpethif->ip_addr);
#if TLS_CONFIG_IPV6
        print_ipaddr(&tmpethif->ip6_addr[0]);
        print_ipaddr(&tmpethif->ip6_addr[1]);
        print_ipaddr(&tmpethif->ip6_addr[2]);
#endif
    }
    break;
    default:
        //printf("UNKONWN STATE:%d\n", status);
        break;
    }
}

void cfg_net_init(void)
{
    printf("---> %s\r\n",__func__);
	button_init(&btn_net_cfg, WM_IO_PA_00, 0);
	button_attach(&btn_net_cfg, SINGLE_CLICK, BTN0_SINGLE_Click_Handler);
	button_attach(&btn_net_cfg, LONG_PRESS_START, BTN0_LONG_PRESS_START_Handler);
	button_start(&btn_net_cfg);

	light_init(&led_net_status, WM_IO_PB_02, 0);
	light_start(&led_net_status);
	light_off(&led_net_status);
    	
    tls_netif_add_status_event(con_net_status_changed_event);
}
