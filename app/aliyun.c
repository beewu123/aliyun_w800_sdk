/*****************************************************************************
*
* File Name : wm_mqtt_cloud.c
*
* Description: mqtt cloud function
*
* Copyright (c) 2015 Winner Micro Electronic Design Co., Ltd.
* All rights reserved.
*
* Author : LiLimin
*
* Date : 2019-3-24
*****************************************************************************/
#include <string.h>
#include "wm_include.h"
#include "wm_netif.h"
#include "aliyun.h"
#include "lwip/netif.h"
#include "wm_sockets.h"
#include "lwip/inet.h"
#include "wm_sockets2.0.3.h"
#include "libemqtt.h"
#include "wm_ntp.h"
#include "token.h"

extern char g_ClientId[128];
extern char g_UserName[128];
extern char g_Password[128];
extern char g_AliyunUrl[128];
extern char g_Aroperty_Topic[128];
extern char g_Get_Topic[128];
// extern char g_Post_Topic[128];
extern char g_Event_Topic[128];

static bool mqtt_cloud_inited = FALSE;
static OS_STK mqtt_cloud_task_stk[MQTT_CLOUD_TASK_SIZE];
static tls_os_queue_t *mqtt_cloud_task_queue = NULL;
static tls_os_timer_t *mqtt_cloud_heartbeat_timer = NULL;

static tls_os_timer_t *mqtt_cloud_postbeat_timer = NULL;


static int mqtt_cloud_socket_id;
static int mqtt_cloud_mqtt_keepalive = 30;
static mqtt_broker_handle_t mqtt_cloud_mqtt_broker;

static uint8_t mqtt_cloud_packet_buffer[MQTT_CLOUD_RECV_BUF_LEN_MAX];

extern struct netif *tls_get_netif(void);
extern int wm_printf(const char *fmt, ...);

static bool mqtt_connect_status = 0;

void set_mqtt_status(bool status)
{
    mqtt_connect_status = status;
}
bool get_mqtt_status()
{
    return mqtt_connect_status;
}

void del_chr( char *s, char ch )
{
    char *t=s; //目标指针先指向原串头
    while( *s != '\0' ) //遍历字符串s
    {
        if ( *s != ch ) //如果当前字符不是要删除的，则保存到目标串中
            *t++=*s;
        s++ ; //检查下一个字符
    }
    *t='\0'; //置目标串结束符。
}


void send_to_cloud(char *data)
{
    
    printf("send_to_cloud:%d\n",strlen(data));
    if(get_mqtt_status()){
        printf("%s\n", data);
        mqtt_publish(&mqtt_cloud_mqtt_broker, (const char *)g_Event_Topic, (const char *)data, strlen(data), 0);
    }
    else{
        printf("--->get_mqtt_status:%d\n", 0);
    }
    
}


static void mqtt_cloud_net_status(u8 status)
{
    struct netif *netif = tls_get_netif();

    switch(status)
    {
    case NETIF_WIFI_JOIN_FAILED:
        wm_printf("--->sta join net failed\n");
        set_mqtt_status(FALSE);
        break;
    case NETIF_WIFI_DISCONNECTED:
        wm_printf("--->sta net disconnected\n");
        set_mqtt_status(FALSE);
        // tls_os_queue_send(mqtt_cloud_task_queue, (void *)MQTT_CLOUD_CMD_END, 0);
        break;
    case NETIF_IP_NET_UP:
        wm_printf("--->sta ip: %v\n", netif->ip_addr.addr);
        tls_os_queue_send(mqtt_cloud_task_queue, (void *)MQTT_CLOUD_CMD_TOKEN, 0);
        break;
    default:
        break;
    }
}

static void mqtt_cloud_heart_timer(void *ptmr, void *parg)
{
    tls_os_queue_send(mqtt_cloud_task_queue, (void *)MQTT_CLOUD_CMD_HEART, 0);
}

static void mqtt_cloud_post_timer(void *ptmr, void *parg)
{
    tls_os_queue_send(mqtt_cloud_task_queue, (void *)MQTT_CLOUD_CMD_POST, 0);
}


static int mqtt_cloud_close_socket(mqtt_broker_handle_t *broker)
{
    int fd = broker->socketid;
    return closesocket(fd);
}

static int mqtt_cloud_send_packet(int socket_info, const void *buf, unsigned int count)
{
    int fd = socket_info;
    return send(fd, buf, count, 0);
}

static int mqtt_cloud_read_packet(int sec, int us)
{
    int ret = 0;

    if ((sec >= 0) || (us >= 0))
    {
        fd_set readfds;
        struct timeval tmv;

        // Initialize the file descriptor set
        FD_ZERO (&readfds);
        FD_SET (mqtt_cloud_socket_id, &readfds);

        // Initialize the timeout data structure
        tmv.tv_sec = sec;
        tmv.tv_usec = us;

        // select returns 0 if timeout, 1 if input available, -1 if error
        ret = select(mqtt_cloud_socket_id + 1, &readfds, NULL, NULL, &tmv);
        if(ret < 0)
            return -2;
        else if(ret == 0)
            return MQTT_CLOUD_READ_TIMEOUT;

    }

    int total_bytes = 0, bytes_rcvd, packet_length;
    memset(mqtt_cloud_packet_buffer, 0, sizeof(mqtt_cloud_packet_buffer));

    if((bytes_rcvd = recv(mqtt_cloud_socket_id, (mqtt_cloud_packet_buffer + total_bytes), MQTT_CLOUD_RECV_BUF_LEN_MAX, 0)) <= 0)
    {
        //printf("%d, %d\r\n", bytes_rcvd, mqtt_cloud_socket_id);
        return -1;
    }
    //printf("recv [len=%d] : %s\n", bytes_rcvd, mqtt_cloud_packet_buffer);
    total_bytes += bytes_rcvd; // Keep tally of total bytes
    if (total_bytes < 2)
        return -1;

    // now we have the full fixed header in mqtt_cloud_packet_buffer
    // parse it for remaining length and number of bytes
    uint16_t rem_len = mqtt_parse_rem_len(mqtt_cloud_packet_buffer);
    uint8_t rem_len_bytes = mqtt_num_rem_len_bytes(mqtt_cloud_packet_buffer);

    //packet_length = mqtt_cloud_packet_buffer[1] + 2; // Remaining length + fixed header length
    // total packet length = remaining length + byte 1 of fixed header + remaning length part of fixed header
    packet_length = rem_len + rem_len_bytes + 1;

    while(total_bytes < packet_length) // Reading the packet
    {
        if((bytes_rcvd = recv(mqtt_cloud_socket_id, (mqtt_cloud_packet_buffer + total_bytes), MQTT_CLOUD_RECV_BUF_LEN_MAX, 0)) <= 0)
            return -1;
        total_bytes += bytes_rcvd; // Keep tally of total bytes
    }

    return packet_length;
}

static int mqtt_cloud_init_socket(mqtt_broker_handle_t *broker, const char *hostname, short port, int keepalive)
{
    int flag = 1;
    struct hostent *hp;

    // Create the socket
    if((mqtt_cloud_socket_id = socket(PF_INET, SOCK_STREAM, 0)) < 0)
        return -1;

    // Disable Nagle Algorithm
    if (setsockopt(mqtt_cloud_socket_id, IPPROTO_TCP, 0x01, (char *)&flag, sizeof(flag)) < 0)
    {
        mqtt_cloud_close_socket(&mqtt_cloud_mqtt_broker);
        return -2;
    }

    // query host ip start
    hp = gethostbyname(hostname);
    if (hp == NULL )
    {
        mqtt_cloud_close_socket(&mqtt_cloud_mqtt_broker);
        return -2;
    }

    struct sockaddr_in socket_address;
    memset(&socket_address, 0, sizeof(struct sockaddr_in));
    socket_address.sin_family = AF_INET;
    socket_address.sin_port = htons(port);
    memcpy(&(socket_address.sin_addr), hp->h_addr, hp->h_length);

    // Connect the socket
    if((connect(mqtt_cloud_socket_id, (struct sockaddr *)&socket_address, sizeof(socket_address))) < 0)
    {
        mqtt_cloud_close_socket(&mqtt_cloud_mqtt_broker);
        return -1;
    }

    // MQTT stuffs
    mqtt_set_alive(broker, mqtt_cloud_mqtt_keepalive);
    broker->socketid = mqtt_cloud_socket_id;
    broker->mqttsend = mqtt_cloud_send_packet;
    //wm_printf("socket id = %d\n", mqtt_cloud_socket_id);
    return 0;
}


static int mqtt_cloud_init()
{
    int packet_length, ret = 0;
    uint16_t msg_id, msg_id_rcv;

    
    wm_printf("step1: init mqtt lib.\r\n");
    mqtt_init(&mqtt_cloud_mqtt_broker, g_ClientId);
    mqtt_init_auth(&mqtt_cloud_mqtt_broker, g_UserName, g_Password);

    printf("g_ClientId = %s\r\n", mqtt_cloud_mqtt_broker.clientid);
    printf("g_UserName = %s\r\n", mqtt_cloud_mqtt_broker.username);
    printf("g_Password = %s\r\n", mqtt_cloud_mqtt_broker.password);


    wm_printf("step2: establishing TCP connection.\r\n");
    ret = mqtt_cloud_init_socket(&mqtt_cloud_mqtt_broker, g_AliyunUrl, MQTT_CLOUD_SERVER_PORT, mqtt_cloud_mqtt_keepalive);
    if(ret)
    {
        wm_printf("init_socket ret=%d\n", ret);
        return -4;
    }

    wm_printf("step3: establishing mqtt connection.\r\n");
    ret = mqtt_connect(&mqtt_cloud_mqtt_broker);
    if(ret)
    {
        wm_printf("mqtt_connect ret=%d\n", ret);
        return -5;
    }

    packet_length = mqtt_cloud_read_packet(MQTT_CLOUD_READ_TIME_SEC, MQTT_CLOUD_READ_TIME_US);
    if(packet_length < 0)
    {
        wm_printf("Error(%d) on read packet!\n", packet_length);
        mqtt_cloud_close_socket(&mqtt_cloud_mqtt_broker);
        return -1;
    }

    if(MQTTParseMessageType(mqtt_cloud_packet_buffer) != MQTT_MSG_CONNACK)
    {
        wm_printf("CONNACK expected!\n");
        mqtt_cloud_close_socket(&mqtt_cloud_mqtt_broker);
        return -2;
    }

    if(mqtt_cloud_packet_buffer[3] != 0x00)
    {
        wm_printf("CONNACK failed!\n");
        mqtt_cloud_close_socket(&mqtt_cloud_mqtt_broker);
        return -2;
    }


    wm_printf("step4: subscribe mqtt\r\n");
    mqtt_subscribe(&mqtt_cloud_mqtt_broker, g_Get_Topic, &msg_id);

    packet_length = mqtt_cloud_read_packet(MQTT_CLOUD_READ_TIME_SEC, MQTT_CLOUD_READ_TIME_US);
    if(packet_length < 0)
    {
        wm_printf("Error(%d) on read packet!\n", packet_length);
        mqtt_cloud_close_socket(&mqtt_cloud_mqtt_broker);
        return -1;
    }

    if(MQTTParseMessageType(mqtt_cloud_packet_buffer) != MQTT_MSG_SUBACK)
    {
        wm_printf("SUBACK expected!\n");
        mqtt_cloud_close_socket(&mqtt_cloud_mqtt_broker);
        return -2;
    }

    msg_id_rcv = mqtt_parse_msg_id(mqtt_cloud_packet_buffer);
    if(msg_id != msg_id_rcv)
    {
        wm_printf("%d message id was expected, but %d message id was found!\n", msg_id, msg_id_rcv);
        mqtt_cloud_close_socket(&mqtt_cloud_mqtt_broker);
        return -3;
    }

    wm_printf("step5: start the Heart-beat preservation timer\r\n");
    ret = tls_os_timer_create(&mqtt_cloud_heartbeat_timer,mqtt_cloud_heart_timer,NULL, (10 * HZ), TRUE, NULL);
    if (TLS_OS_SUCCESS == ret)
        tls_os_timer_start(mqtt_cloud_heartbeat_timer);


    ret = tls_os_timer_create(&mqtt_cloud_postbeat_timer,mqtt_cloud_post_timer,NULL, (10 * HZ), TRUE, NULL);
    if (TLS_OS_SUCCESS == ret)
        tls_os_timer_start(mqtt_cloud_postbeat_timer);


    /* step6: push mqtt subscription (when a subscription message is received) */

    return 0;
}

static int mqtt_cloud_loop(void)
{
    int packet_length = 0;
    int counter = 0;

    counter++;
    packet_length = mqtt_cloud_read_packet(0, 1);
    if(packet_length > 0)
    {
        //wm_printf("recvd Packet Header: 0x%x...\n", mqtt_cloud_packet_buffer[0]);

        if (MQTTParseMessageType(mqtt_cloud_packet_buffer) == MQTT_MSG_PUBLISH)
        {
            uint8_t topic[100], *msg;
            uint16_t len;
            len = mqtt_parse_pub_topic(mqtt_cloud_packet_buffer, topic);
            topic[len] = '\0'; // for printf
            len = mqtt_parse_publish_msg(mqtt_cloud_packet_buffer, &msg);
            msg[len] = '\0'; // for printf
            wm_printf("recvd: %s >>> %s\n", topic, msg);

            mqtt_publish(&mqtt_cloud_mqtt_broker, (const char *)g_Event_Topic, (const char *)msg, len, 0);
            wm_printf("pushed: %s <<< %s\n", g_Event_Topic, msg);
        }

        tls_os_queue_send(mqtt_cloud_task_queue, (void *)MQTT_CLOUD_CMD_LOOP, 0);
    }
    else if (packet_length == MQTT_CLOUD_READ_TIMEOUT)
    {
        tls_os_queue_send(mqtt_cloud_task_queue, (void *)MQTT_CLOUD_CMD_LOOP, 0);
    }
    else if(packet_length == -1)
    {
        set_mqtt_status(FALSE);
        wm_printf("mqtt error:(%d), stop mqtt cloud!\n", packet_length);
        tls_os_timer_stop(mqtt_cloud_heartbeat_timer);
        mqtt_cloud_close_socket(&mqtt_cloud_mqtt_broker);
        tls_os_time_delay(HZ*5);
        tls_os_queue_send(mqtt_cloud_task_queue, (void *)MQTT_CLOUD_CMD_TOKEN, 0);
    }

    return 0;
}

static void mqtt_cloud_task(void *p)
{
    int ret;
    void *msg;
    struct tls_ethif *ether_if = tls_netif_get_ethif();



    if (ether_if->status)
    {
        wm_printf("--->sta ip: %v\n", ether_if->ip_addr.addr);
        tls_os_queue_send(mqtt_cloud_task_queue, (void *)MQTT_CLOUD_CMD_TOKEN, 0);
    }

    for ( ; ; )
    {
        ret = tls_os_queue_receive(mqtt_cloud_task_queue, (void **)&msg, 0, 0);
        if (!ret)
        {
            switch((u32)msg)
            {
            case MQTT_CLOUD_CMD_START:
                do
                {

                    // extern int tls_get_mac_addr(u8 *mac);
                    // u8 mac[6]={0};
                    // tls_get_mac_addr(mac);
                    // char client_id[50] = {'\0'};
                    // sprintf(client_id, "WM_%02X%02X%02X%02X%02X%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
                    // ret = mqtt_cloud_init(client_id);
                    
                    ret = mqtt_cloud_init();
                    // wm_printf("--->mqtt_cloud_init:%d\r\n",ret);
                    if (ret == 0){
                        set_mqtt_status(TRUE);
                        tls_os_queue_send(mqtt_cloud_task_queue, (void *)MQTT_CLOUD_CMD_LOOP, 0);
                        break;
                    }
                    else{
                        set_mqtt_status(FALSE);
                        tls_os_time_delay(HZ);
                        tls_os_queue_send(mqtt_cloud_task_queue, (void *)MQTT_CLOUD_CMD_TOKEN, 0);
                        break;
                    }
                }
                while (0);
                break;
            case MQTT_CLOUD_CMD_HEART:
                wm_printf("send heart ping\r\n");
                mqtt_ping(&mqtt_cloud_mqtt_broker);
                break;
            case MQTT_CLOUD_CMD_POST:
                {
                    struct tls_curr_bss_t *bss = tls_mem_alloc(sizeof(struct tls_curr_bss_t));
                    if(!bss)
                    {
                       printf("malloc failed\r\n");
                    }
                    memset(bss, 0, sizeof(struct tls_curr_bss_t));

                    tls_wifi_get_current_bss(bss);
                    char json_str[512] = {0};
                    u16  len = 0;
                    len+=sprintf(json_str+len,"{");
                        len+=sprintf(json_str+len, "\"id\":\"W800\",");
                        len+=sprintf(json_str+len, "\"version\":\"1.0\",");
                        len+=sprintf(json_str+len, "\"params\":");
                            len+=sprintf(json_str+len,"{");
                            len+=sprintf(json_str+len, "\"WiFiRSSI\":-%d", bss->rssi);
                            len+=sprintf(json_str+len,"}");
                    len+=sprintf(json_str+len,"}");

                    send_to_cloud(json_str);
                    tls_mem_free(bss);
                }
                break;
            case MQTT_CLOUD_CMD_LOOP:
                mqtt_cloud_loop();
                break;
            case MQTT_CLOUD_CMD_TOKEN:
                do{
                    
                    u32 timestamp = tls_ntp_client();
                    timestamp -= 3600*8;
                    // printf("--->timestamp:%d\n",timestamp);
                    build_mqtt_auth(timestamp);   
                }while(0);
                tls_os_queue_send(mqtt_cloud_task_queue, (void *)MQTT_CLOUD_CMD_START, 0);
                break;
            // case MQTT_CLOUD_CMD_END:
            //     tls_os_timer_stop(mqtt_cloud_heartbeat_timer);
            //     mqtt_cloud_close_socket(&mqtt_cloud_mqtt_broker);
            //     break;
            default:
                break;
            }
        }
    }
}


int aliyun_task_create(void)
{
    if (!mqtt_cloud_inited)
    {
        tls_os_task_create(NULL, "aliyun", mqtt_cloud_task,
                           NULL, (void *)mqtt_cloud_task_stk,  /* task's stack start address */
                           MQTT_CLOUD_TASK_SIZE * sizeof(u32), /* task's stack size, unit:byte */
                           MQTT_CLOUD_TASK_PRIO, 0);

        tls_os_queue_create(&mqtt_cloud_task_queue, MQTT_CLOUD_QUEUE_SIZE);

        tls_netif_add_status_event(mqtt_cloud_net_status);

        mqtt_cloud_inited = TRUE;
    }

    return WM_SUCCESS;
}



