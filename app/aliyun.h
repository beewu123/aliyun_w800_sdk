/*****************************************************************************
*
* File Name : aliyun.c
*
* Description: mqtt cloud function
*
* Copyright (c) 2015 Winner Micro Electronic Design Co., Ltd.
* All rights reserved.
*
* Author : LiLimin
*
* Date : 2019-3-24
*****************************************************************************/
#ifndef __ALIYUN_H__
#define __ALIYUN_H__
#include <string.h>
#include "wm_include.h"


#define MQTT_CLOUD_TASK_PRIO             39
#define MQTT_CLOUD_TASK_SIZE             512
#define MQTT_CLOUD_QUEUE_SIZE            6

#define MQTT_CLOUD_RECV_BUF_LEN_MAX      1024

#define MQTT_CLOUD_CMD_START             0x1
#define MQTT_CLOUD_CMD_HEART             0x2
#define MQTT_CLOUD_CMD_LOOP              0x3
#define MQTT_CLOUD_CMD_TOKEN             0x4
#define MQTT_CLOUD_CMD_POST              0x5

#define MQTT_CLOUD_READ_TIMEOUT        (-1000)

#define MQTT_CLOUD_READ_TIME_SEC         3
#define MQTT_CLOUD_READ_TIME_US          0

// #define MQTT_CLOUD_SERVER_ADDR          "iot-06z00du5j0qe9pi.mqtt.iothub.aliyuncs.com" // aliyun
// #define MQTT_CLOUD_SERVER_ADDR          "isme.fun"                                  // EMQTT
#define MQTT_CLOUD_SERVER_PORT           1883

int aliyun_task_create(void);


void send_to_cloud(char *data);

void del_chr( char *s, char ch );






#endif