# W80X系列接入阿里云物联网平台SDK DEMO
## 注:（本SDK仅供参考学习）
#### 介绍
```
基于联盛德官网W800 SDK，通过使用MQTT.fx接入阿里物联网平台
1.移植MultiButton按键库，拓展LED库
2.移植aliyun yoc sdk中key-value库
3.增加AT指令烧写设备证书（即 ProductKey 、ProductSecret、DeviceSecret）
```

#### 使用说明
```
详情见联盛德问答社区文章 
http://ask.winnermicro.com/article/59.html

由此链接注册送自动送10论坛积分
http://ask.winnermicro.com/invite/2

```

#### 参与贡献
```
MultiButton（多功能按键库）
https://github.com/0x1abin/MultiButton

联盛德 W800 SDK
https://www.winnermicro.com/html/1/156/158/558.html

使用MQTT.fx接入物联网平台
https://help.aliyun.com/document_detail/86706.html#title-qro-mpx-cqc

阿里云生活物联网平台(阿里飞燕)（key-value库）
https://occ.t-head.cn/vendor/detail/index?spm=a2cl5.26127479.0.0.20c6180fpiTSO6&id=3828588859215056896&vendorId=3717904810151473152&module=1
```