/***************************************************************************** 
* 
* File Name : cfg_net.c
* 
* Description: 注册配置网络模块 
* 
* Copyright (c) 2014 Winner Micro Electronic Design Co., Ltd. 
* All rights reserved. 
* 
* Author : isme
* 
* Date : 2022-5-17
*****************************************************************************/ 
#ifndef __CFG_NET_H__
#define __CFG_NET_H__

#include <string.h>
#include "wm_include.h"

void cfg_net_init(void);

#endif /*__CFG_NET_H__*/
