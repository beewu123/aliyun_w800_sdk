/***************************************************************************** 
* 
* File Name : main.c
* 
* Description: main 
* 
* Copyright (c) 2014 Winner Micro Electronic Design Co., Ltd. 
* All rights reserved. 
* 
* Author : isme
* 
* Date : 2022-5-17
*****************************************************************************/ 
#include "wm_include.h"
#include "aliyun.h"
#include "cfg_net.h"
#include "component/kv/include/aos/kv.h"

int get_aliyun_license(void)
{
	printf("---> %s\r\n",__func__);
    char pk[32 + 1];
    char dn[20 + 1];
    char ds[64 + 1];
    // char ps[64 + 1];
    // char pid[16 + 1];
    int size = 0;
    int ret = 0;

    size = sizeof(pk);
    ret = aos_kv_get("hal_devinfo_pk", pk, &size);
    if(ret != 0) {
		return -1;
	}
    size = sizeof(dn);
    ret = aos_kv_get("hal_devinfo_dn", dn, &size);
    if(ret != 0) {
		return -1;
	}
    size = sizeof(ds);
    ret = aos_kv_get("hal_devinfo_ds", ds, &size);
    if(ret != 0) {
		return -1;
	}

	return ret;
}

void UserMain(void)
{
	extern int tls_get_mac_addr(u8 *mac);
	extern int tls_get_bt_mac_addr(u8 *mac);
	extern u32 tls_mem_get_avail_heapsize(void);
	u8 wifi_mac[6] = {0};
	u8 ble_mac[6] = {0};
	tls_get_mac_addr(wifi_mac);
	tls_get_bt_mac_addr(ble_mac);
	printf("             \\\\\\|///\n");
	printf("           \\\\  .-.-  //\n");
	printf(".           (  .@.@  )  \n");
	printf("+-------oOOo-----(_)-----oOOo---------+\n\n");
	printf(" ---> Compile "__DATE__", "__TIME__"\n");
	printf(" ---> %s.c\r\n",__func__);
	printf(" ---> GetHeap:%d\n",tls_mem_get_avail_heapsize());
	printf(" ---> BLE  MAC:%02X%02X%02X%02X%02X%02X\n",ble_mac[0],ble_mac[1],ble_mac[2],ble_mac[3],ble_mac[4],ble_mac[5]);
	printf(" ---> WIFI MAC:%02X%02X%02X%02X%02X%02X\n",wifi_mac[0],wifi_mac[1],wifi_mac[2],wifi_mac[3],wifi_mac[4],wifi_mac[5]);
	printf("\n+---------------------Oooo------------+\n");
	printf("\n user task \n");

	aos_kv_init("kv");

	if(get_aliyun_license() != 0)
	{
		printf("---> get aliyun license failed\r\n");
		return;
	}

	cfg_net_init();

	aliyun_task_create();

#if DEMO_CONSOLE
	CreateDemoTask();
#endif
//用户自己的task
}

