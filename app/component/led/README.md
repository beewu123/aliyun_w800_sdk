# Example
```
//初始化结构体
struct Light led0;
struct Light led1;
struct Light led2;

void UserMain(void)
{
	printf("\n user task \n");
	printf("--->Compile "__DATE__", "__TIME__"\r\n\r\n");

	// led 初始化IO
	light_init(&led0, WM_IO_PB_00, 0);
	light_init(&led1, WM_IO_PB_01, 0);
	light_init(&led2, WM_IO_PB_02, 0);

	// led 加入队列
	light_start(&led0);
	light_start(&led1);
	light_start(&led2);

	// led0 亮5s后熄灭
	light_on(&led0);
	tls_os_time_delay(HZ);
	light_off(&led0);

	// led1 闪烁5s后熄灭
	light_blink(&led1, 100, 100);
	tls_os_time_delay(5*HZ);
	light_off(&led1);

	// led2 闪烁5s后熄灭
	light_blink_timeout(&led2, 100, 100, 5000);

	// led 移除队列
	light_stop(&led0);
	light_stop(&led1);
	light_stop(&led2);
}
```